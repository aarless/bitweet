#!/usr/bin/env python
# -*- coding: utf-8 -*-

from requests import get
import json
import os
import tweepy

# Credenciais do aplicativo do Twitter
CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_KEY = ''
ACCESS_SECRET = ''
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

open('bit.log', 'a').close() #cria o arquivo de log, caso nao exista

data = get('https://api.coindesk.com/v1/bpi/currentprice/BRL.json').json()
bit = data['bpi']['BRL']['rate'] 

log = open("bit.log", "r").readline() #le arquivo de log

msg = "Bitcoin: R$ " + bit

#compara o ip com o log
if bit != log:
	api.update_status(msg)

#salva o ip atual no log
log = open('bit.log', 'w')
log.write(str(bit))
log.close()